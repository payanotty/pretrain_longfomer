FROM gcr.io/kaggle-gpu-images/python:latest

WORKDIR /pretrain

# RUN set -xe \
#     && apt-get update \
#     && apt-get -y install python-pip
RUN pip install --upgrade pip
RUN pip install pytorch_lightning
RUN pip install tvm
RUN pip install transformers
RUN git clone https://github.com/NVIDIA/apex
RUN cd apex
RUN cd apex \
    && pip install -v --disable-pip-version-check --no-cache-dir --global-option="--cpp_ext" --global-option="--cuda_ext" ./
#RUN ls -l
#RUN python /pretrain/setup.py install
#RUN sh /pretrain/install_apex.sh
