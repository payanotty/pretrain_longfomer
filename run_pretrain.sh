python scripts/pretrain.py &&
  --input_dir ./data &&
  --seqlen 2048 &&
  --tokenizer ./extended_tokenizer &&
  --model allenai/longformer-base-4096 &&
  --save_dir ./models/ &&
  --save_prefix longformer_base_added &&
  --lr 5e-5 &&
  --train_steps 20000 &&
  --warmup_steps 1200 &&
  --batch_size 4 &&
  --grad_accu 8 &&
  --fp16 True &&
  --num_workers 4

